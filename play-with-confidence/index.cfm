<cfsetting showdebugoutput="yes">

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name="robots" content="index,follow">
		<meta name="revisit-after" content="14 days">
		<meta name="googlebot" content="index,follow">
		<meta charset="UTF-8">
		<cfset request.metaTitle = "STX Women's Lacrosse - Play with Confidence and Defend the Game">
		<cfset request.metaDesc = "Defend the Game with STX Women's Lacrosse. Win an unreleased complete stick &amp; other STX prizes. Find out more about what's coming for defenders this November.">
		<meta property="og:type" content="website">
		<meta property="og:title" content=""/>
		<meta property="og:description" content="Defend the Game with STX. Coming November 2016. Click to find out how you can win an unreleased stick and more STX prizes. www.stx.com/play-with-confidence" />
		<meta property="og:url" content="http://dev.stx.com/play-with-confidence/" />
		<cfset request.metaImage = "http://dev.stx.com/play-with-confidence/images/fortress-600-share-fbimage.jpg"/>

		<meta name="viewport" content="width=device-width, initial-scale=.8">
	    <link rel="stylesheet" href="/css/styles.css" />
	    <link rel="stylesheet" href="/play-with-confidence/css/styles.css" />
	    <link href="/play-with-confidence/css/jquery.bxslider.css" rel="stylesheet" />
	    <link rel="stylesheet" href="/css/jquery.prettyPhoto.css" />
		
		<!--- Font: Tungsten ---> 
		<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6574652/7208772/css/fonts.css" />
		<!--- Font: Klavika ---> 
		<script src="https://use.typekit.net/rzs0lgc.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	    <script src="/js/jquery.prettyPhoto.js"></script>        
		<script language="javascript" type="text/javascript" src="/js/jquery.masonry.min.js"></script>
        <script language="javascript" type="text/javascript" src="/js/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="/play-with-confidence/js/scripts.js"></script>
		<script language="javascript" type="text/javascript" src="/js/scripts.js"></script>
	    <script type="text/javascript">
	    	$(function(){
		    	$("a[rel^='prettyPhoto']").prettyPhoto({
			    	'deeplinking':false,
			    	'social_tools':false,
			    	'default_width': 640,
			    	'default_height': 360
		    	});
	    	});
	    </script>
		<!--[if lt IE 8]>
		<script src="/ie7/IE8.js" type="text/javascript"></script>
		<![endif]-->
		<!--[if lt IE 7]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js">IE7_PNG_SUFFIX=".png";</script>
		<![endif]-->
		<!--generate all new elements for Internet Explorer-->
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body class="play-with-confidence page-<cfoutput>#url.page#</cfoutput>">
    	<cfinclude template="/includes/menu_trigger.cfm">
	  <div id="overlay"></div>
	<div id="wrapped">
		<cfinclude template="/includes/header_mobile.cfm">
		<cfset usehash = false>
		<cfset request.gender = 'womens'>
		<cfset request.genderid = 1>
		<div id="wrapper">
			<cfinclude template="/includes/header.cfm">
		</div>
		<div id="main">
			<cfinclude template="display/dsp_#url.page#.cfm">
		</div>		
		<div id="wrapper" class="mobile-hide">	
			<cfinclude template="/includes/footer.cfm">
			<div class="clearfix"></div>
		</div>
	</div>
		<cfinclude template="/includes/mobile-menu.cfm">
		<cfinclude template="/modules/mod_meta.cfm">
	</body>
	<script type="text/javascript" src="/play-with-confidence/js/html5lightbox.js"></script>
	<script language="javascript" type="text/javascript" src="/play-with-confidence/js/jquery.cycle2.min.js"></script>
	<script type="text/javascript">
        $(window).resize(function() {
            checkWindowSize();
        });
        checkWindowSize();
        function checkWindowSize() {
            if ($(window).width() <= 640) {
                jQuery("a[rel^='prettyPhoto']").unbind('click');
            } else {
                $("a[rel^='prettyPhoto']").prettyPhoto({
                    social_tools: false,
                    deeplinking: false
                });
                jQuery("a[rel^='prettyPhoto']").bind('click');
            }
        }
    </script>
</html>