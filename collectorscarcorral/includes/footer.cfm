<footer class="footer">
	<div class="top">
		<img src="images/footer-logo.png" alt="Collectors Car Corral">
	</div>
	<div class="columns group">
		<div class="col">
			<h4>Find Us At</h4>
			<p>
				10 Music Fair Road<br>
				Owings Mills, MD 21117
			</p>
		</div>
		<div class="col">
			<h4>Business Hours</h4>
			<p><strong>By Appointment Only</strong></p>
			<p>
				Monday - Friday<br>
				8:00am - 6:00pm
			</p>
			<p>
				Saturday - Sunday<br>
				9:00am - 5:00pm
			</p>
		</div>
		<div class="col">
			<h4>Contact Us</h4>
			<p style="white-space: nowrap;">
				<strong>Office:</strong> <a href="tel:410-363-0400" class="link-tel">410-363-0400</a><br>
				<strong>Email:</strong> <a href="mailto:info@collectorscarcorral.com">info@collectorscarcorral.com</a>
			</p>
		</div>
	</div>
	<div class="btm">
		<ul class="social">
<!---
			<li>
				<a href="" target="_blank">
					<span class="fa-stack">
					  <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
						<i class="fa fa-instagram fa-stack-1x"></i>
  				</span>
				</a>
			</li>
--->
			<li>
				<a href="https://www.facebook.com/CollectorsCarCorral.CCC/" target="_blank">
					<span class="fa-stack">
					  <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
						<i class="fa fa-facebook fa-stack-1x"></i>
  				</span>
				</a>
			</li>
<!---
			<li>
				<a href="" target="_blank">
					<span class="fa-stack">
					  <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
						<i class="fa fa-google-plus fa-stack-1x"></i>
  				</span>
				</a>
			</li>
--->
		</ul>
		<div class="copyright">
			Copyright &copy; 2016 Collectors Car Corral
		</div>
	</div>
</footer>