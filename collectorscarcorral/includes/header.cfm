<header class="header">
	<div class="header-top group">
		<a href="/" class="logo"><img src="images/nav-logo.png" alt="Collectors Car Corral"></a>
		<ul class="nav-utility">
			<li><a href="/news-events">News & Events</a></li>
			<li><a href="/vehicles-for-sale" class="link-vehicles">Vehicles for Sale <i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
			<li><a href="tel:410-363-0400" class="link-phone"><i class="fa fa-phone fa-lg" aria-hidden="true"></i> 410-363-0400</a></li>
		</ul>
	</div>
  <div class="nav-main">
	  <div class="nav-main-int">
		  <a href="/" class="logo-sticky"><img src="images/nav-logo-sticky.png" alt="Collectors Car Corral"></a>
		  <ul class="nav-utility-sticky">
				<li><a href="/news-events">News & Events</a></li>
				<li><a href="/vehicles-for-sale" class="link-vehicles">Vehicles for Sale <i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
			</ul>
		  <ul class="nav">
			<li><a href="/about-us">About</a></li>
			<li><a href="/showroom">Our Showroom</a></li>
			<li><a href="/services-features">Services & Features</a></li>
			<li><a href="/pricing">Pricing</a></li>
			<li><a href="/contact-us">Contact</a></li>
		  </ul>	  
	  </div>
  </div>
</header>

<header class="header-mobile">
	<div class="wrap group">
		<a href="/" class="logo"><img src="images/nav-mobile-logo.png" alt="Collectors Car Corral"></a>
		<a class="mobile-nav-toggle"></a>
	</div>
</header>

<div class="mobile-nav">
	<a class="mobile-nav-toggle"></a>
	<ul class="nav">
		<li><a href="/"><img src="images/mobile-logo.png" class="mobile-logo" alt="Collectors Car Corral"></a></li>
		<li><a href="/about-us">About</a></li>
		<li><a href="/showroom">Our Showroom</a></li>
		<li><a href="/services-features">Services & Features</a></li>
		<li><a href="/pricing">Pricing</a></li>
		<li><a href="/contact-us">Contact</a></li>
		<li class="link-small"><a href="/news-events">News & Events</a></li>
		<li class="link-small"><a href="/vehicles-for-sale">Vehicles for Sale</a></li>
  </ul>	
</div>