$(document).ready(function() {
	
  ////////////////////////////////////////////
  // sticky nav
  
	var stickyNavTop = $('.nav-main').offset().top;
	var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		      
		if (scrollTop > stickyNavTop) { 
			$('.nav-main').addClass('sticky');
		} else {
		  $('.nav-main').removeClass('sticky'); 
		}
	};
	 
	stickyNav();
	 
	$(window).scroll(function() {
	  stickyNav();
	});
	

  ////////////////////////////////////////////
  // mobile nav

  //add arrow spans to all top level mobile nav
  //$(".mobile-nav > ul > li > a").append('<span class="arrow"></span>');

  //add overview item to subnavs and setup accordions
  /*
	  $(".mobile-nav ul.subnav").each(function(){
    var $parentAnchor = $(this).siblings('a');
    $(this).prepend('<li><a href="' + $parentAnchor.attr('href') + '">Overview</a></li>');

    $parentAnchor.click(function(e){
      e.preventDefault();
      $(this).toggleClass("expanded");
      $(this).siblings('ul').slideToggle('fast');
    });
  });
  */

  //toggle
  var animating = false;

  function openMobileNav()
  {
    $(".mobile-nav-toggle").toggleClass("open");
    $(".mobile-nav").css({display:'block'}).stop().animate({opacity: 1, left: 0}, 350, function(){
      animating = false;
    });
  }

  function closeMobileNav()
  {
    $(".mobile-nav-toggle").toggleClass("open");
    $(".mobile-nav").stop().animate({opacity: 0, left: "100%"}, 350, function(){
      $(".mobile-nav").css({display:'none'});
      animating = false;
    });
  }
	
	
  $(".mobile-nav-toggle").click(function(e){
    e.preventDefault();
    if(!animating)
    {
      animating = true;
      if($(".mobile-nav-toggle").hasClass("open")) closeMobileNav();
      else openMobileNav();
    }
  });

  //sizing
  function sizeMobileNav()
  {
    $(".mobile-nav").css({height: $("body").height(), minHeight: "100%"});
  }
  $(window).resize(sizeMobileNav);
  sizeMobileNav();
  
  
  // Binding featured image links to lightbox on showroom box
  // Lightbox plugin: Swipebox
  // Lightbox URI: http://brutaldesign.github.io/swipebox
  
  $( '.galleries .item' ).click( function( e ) {
	  	e.preventDefault();
		var galleryid = $(this).data('galleryid');
		var images = $(".swipebox[rel=gallery-"+galleryid+"]");
		var gallery = [];
		
		$.each(images, function(index, value){ 
			var imageHref = $(value).attr("href"); 
			var imageTitle = $(value).attr("title");
			
			gallery.push({href: imageHref, title: imageTitle });
			
		});
		
		$.swipebox(gallery);
	});
  
	
});


$(window).scroll(function() {
	
	function isScrolledIntoView(elem){
	
	  var docViewTop = $(window).scrollTop();
	  var docViewBottom = docViewTop + $(window).height();
	
	  var elemTop = $(elem).offset().top;
	  var elemBottom = elemTop + $(elem).height() + 200;
	
	  return (((elemBottom <= docViewBottom) && (elemTop >= docViewTop)) || $(window).height() < 500);
	    
	}	

	if(document.getElementById('home')) {
		if($(window).width() > 960){
		  if (isScrolledIntoView($(".home .showroom"))) {
		  	$(".home .showroom .img").addClass("load");
		  }
		}
	}
});