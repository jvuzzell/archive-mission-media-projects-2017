<div id="bg-contact" class="angle hdr-img bg-img" style="background-image:url(../images/bg-contact-lg.png);">
</div>
<div class="contact-container">
<!--- 	<img src="images/bg-contact-mobile-640px.jpg" class="bg-contact-mobile"> --->
<!--- 	<img src="images/bg-contact-mobile-480px.jpg" class="bg-contact-mobile"> --->
	<section class="contact">
		<div class="contact-int">
			<h1>Contact Us</h1>
			<div class="subtitle">All Hours By Appointment Only</div>
			<div class="contact-info">
				<p class="hours">
					Monday - Friday 8:00 am - 6:00 pm<br>
					Saturday - Sunday 9:00 am - 5:00 pm
				</p>
				<div class="location group">
					<div class="col left">
						10 Music Fair Road<br>
						Owings Mills, MD 21117<br>
						<a href="">Get Directions <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
					</div>
					<div class="col right">
						<a href="tel:410-363-0400"><img src="images/telephone-icon.png"/> 410-363-0400</a><br>
						<a href="mailto:info@collectorscarcorral.com"><img src="images/email-icon.png"/> info@collectorscarcorral.com</a><br>
					</div>
				</div>
			</div>
			<form name="contactForm" method="post">
				<div class="error-msg">Oops! Please fill out the required fields.</div>
				<input type="text" value="Full Name*" name="name"><br>
				
				<input type="text" value="Email Address*" name="email" class="error"><br>
				<input type="text" value="Phone Number" name="phone"><br>
				<input type="text" value="Subject" name="subject"><br>
				<textarea rows="10">MESSAGE*</textarea><br>
				<input type="submit" value="Submit" name="submit" class="btn">
			</form>
		</div>
	</section>
	<div class="push-footer"></div>
</div>
