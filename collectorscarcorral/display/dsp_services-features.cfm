<div class="services-features">
	<section class="angle hdr-img bg-img" style="background-image:url(../images/ccc-2016-facility-10.jpg);">

	</section>
	
	<section class="page-content">
		<div class="wrap">	
			<div class="intro">
				<h1>Our <span>Features</span></h1>
				<p>Our Storage Facility Features Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
			</div>
		</div>
	</section>
	
<section class="features">
	<div class="wrap">

		<div class="features-grid">
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-climate.png);"></div>
				<div class="desc">
					<h2>Climate Controlled Facility</h2>
					<p>State of the Art Climate Controlled Facility with an Insulated Roof and Ventilation System</p>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-secure.png);"></div>
				<div class="desc">
					<h2>Highly Secure Building</h2>
					<p>Monitored Commercial Grade Security System</p>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-storage.png);"></div>
				<div class="desc">
					<h2>25,000 Sq Ft Storage Facility</h2>
					<p>Various Storage Sizes to Fit Your Needs</p>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-livestream.png);"></div>
				<div class="desc">
					<h2>Live Video Stream</h2>
					<p>24 Hour Access to Live Video Stream of Your Vehicle</p>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-fire.png);"></div>
				<div class="desc">
					<h2>Fire Protection System</h2>
					<p>Centrally Monitored Fire Protection System</p>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-concierge.png);"></div>
				<div class="desc">
					<h2>Professional Concierge Services</h2>
					<p>Courteous and Profesional Staff Members</p>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-checkin.png);"></div>
				<div class="desc">
					<h2>Quick Check In / Check Out Process</h2>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-standards.png);"></div>
				<div class="desc">
					<h2>Quality Building Standards</h2>
					<p>Sealed Concrete Flooring and Abundant Indoor Lighting</p>
				</div>
			</div>
			<div class="item">
				<div class="icon" style="background-image: url(images/icon-hours.png);"></div>
				<div class="desc">
					<h2>Convenient Location & Hours of Operation</h2>
				</div>
			</div>
		</div>
		<div class="btn-links">
			<a href="/pricing" class="btn">View Packages & Pricing</a>
			<a href="/contact-us" class="btn btn-transparent">Schedule an Appointment</a>
		</div>
	</div>
</section>

<section class="services features-bg">
	<div class="wrap">
		<div class="col full-width">
			<h1>Additional Services</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporincid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.</p>
			<a href="/pricing#additonal_services" class="btn btn-white">Learn More</a>
		</div>
<!---
		<div class="col col-left">
			<h1>Additional Services</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporincid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.</p>
			<a href="" class="btn btn-white">Learn More</a>
		</div>

		<div class="col col-right">
			<h2>Services Include:</h2>
			<ul>
				<li>Monthly Vehicle Start</li>
				<li>Additional Items for Purchase</li>
				<li>Wash/Detailing</li>
				<li>Paintless Dent Repair</li>
				<li>Use of Premium Trickle Charger</li>
			</ul>
		</div>
--->
	</div>
</section>
</div>