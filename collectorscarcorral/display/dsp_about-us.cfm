<div class="about-us">
		<div class="repeat-bg">	
		<section class="hdr-img">
			<img class="mobile-hide" src="images/hdr-about-us.png">
			<img class="mobile" src="images/hdr-about-us-mobile.png">
		</section>

		<section class="page-content">
			<div class="wrap">	
				<div class="intro">
					<h1>About <span>Us</span></h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris egestas faucibus magna et luctus. Donec non risus congue, vehicula magna id, blandit arcu. Nam ligula sem, placerat a ultrices pulvinar, ornare et justo. Sed vel purus diam. Fusce ornare bibendum dignissim. Vivamus dapibus eget sapien ut tristique. Cras et fermentum dui. Pellentesque est augue, auctor quis dolor sit amet, turpis. </p>
					<p>
	Nulla libero justo, laoreet id orci volutpat, elementum volutpat justo. Nullam quis euismod ex. Aenean auctor libero suscipit erat interdum hendrerit. Donec a pretium diam.</p>
				</div>
			</div>
		</section>
	</div>
	
	<section class="sponsors">
		<div class="wrap">
			<div class="grid">
				<div class="item"><img src="http://placehold.it/400x400"/></div>
				<div class="item"><img src="http://placehold.it/400x400"/></div>
				<div class="item"><img src="http://placehold.it/400x400"/></div>
				<div class="item"><img src="http://placehold.it/400x400"/></div>

			</div>
		</div>
	</section>
	
	<div class="repeat-bg">
		<div class="testimonials"> 
			<div class="wrap">
				<div class="intro">
					<h1>Testimonials</h1>
					<div class="icon" style="background-image: url(images/quotation-mark.png);"></div>
				</div>
						
				<div class="slider-testimonials cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="100000" data-cycle-slides="> .slide">
					<div class="slide">
						<div class="content">
							<p class="quote">Working with Collectors Car Corral has been amazing. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<p class="byline"><span>John Smith</span>, Client of 10 years</p>
						</div>
					</div>
					<div class="slide">
						<div class="content">
							<p class="quote">Working with Collectors Car Corral has been amazing. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<p class="byline"><span>John Smith</span>, Client of 10 years</p>
						</div>
					</div>
					<div class="slide">
						<div class="content">
							<p class="quote">Working with Collectors Car Corral has been amazing. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<p class="byline"><span>John Smith</span>, Client of 10 years</p>
						</div>
					</div>
						<div class="cycle-prev"></div>
						<div class="cycle-next"></div>
				</div>

				<div class="btn-links">
					<a href="/services-features" class="btn">About Our Services</a>
				</div>	
			</div>
		</div>
	</div>
	
	<section class="news">
		<div class="wrap">	
			<div class="intro">
				<h1>In the <span>News</span></h1>
			</div>
			<div class="border-shadow">
				<div class="article">
					<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
					<div class="content">
						<h3>Classic Car Weekend</h3>
						<h5>May 16 - 22, 2016</h5>
						<p>
						This popular car show features over 3,400 hot rods, customs, classics, street machines, muscle cars and more. Live entertainment, celebrity guests, special attractions, boardwalk parades, manufacturers vendor midway and more. Admission. Boardwalk Parades on Thursday, Friday, and Saturday mornings from 7am to 10am. So many things to see and do, so make sure you cruise on down to Ocean City, MD, for the 25th Annual Cruisin' Ocean City. 
						</p>
						<h6>For additional information, visit www.cruisinoceancity.com, or call 410-289-2800 or 800-626-2326</h6>
						<a class="cta" href="">Visit Website</a>
					</div>
				</div>
			</div>
			
		</div> 
		<div class="btn-links">
			<a href="/news-events" class="btn btn-transparent">More News and Events</a>
		</div>	
	</section>


	<section class="push-footer">&nbsp;</section>
	
</div>