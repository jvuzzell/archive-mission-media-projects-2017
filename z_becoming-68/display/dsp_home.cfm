<cfset twitterDescription="https://twitter.com/home?status=Go%20inside%20the%20lives%20of%20America%27s%20Top%2068%20young%20hockey%20players%20with%20STX%20Hockey%20and%20watch%20the%20Becoming%2068%20series!%20http%3A%2F%2Fwww.stx.com%2Fbecoming-68">
<cfset subject="Becoming%2068%20by%20STX!">
<cfset body="Go%20inside%20the%20lives%20of%20America%27s%20Top%2068%20young%20hockey%20players%20with%20STX%20Hockey%20and%20watch%20the%20Becoming%2068%20series!%0A%0Ahttp%3A%2F%2Fwww.stx.com%2Fbecoming-68">

<cfparam name="msg" default="">
<cfparam name="success" default="0">

<cfif isDefined('form.email')>
	<cfif form.contact_name is not "">
		<cfset msg = "Invalid Submission">
	
    
    <cfelse>
		<cfsavecontent variable="localscope.soapRequest">
		<cfoutput>
      <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
          <Subscriber_Add_Subscriber xmlns="http://BlueSkyFactory.Publicaster7.LegacyAPI/Subscriber/">
            <AccountID>L05p4NgaQEw=</AccountID>
            <PrimaryKey>Email</PrimaryKey>
            <Action>AppendAndUpdate</Action>
            <ListIDs>
              <int>1</int>
            </ListIDs>
            <IncludeDeleted>0</IncludeDeleted>
            <FieldNames>
              <string>Email</string>
              <string>Source</string>
              <string>ice_hockey</string>
              <string>becoming_68</string>
            </FieldNames>
            <FieldValues>
              <string>#form.email#</string>
              <string>becoming_68</string>
              <string>1</string>
              <string>1</string>
            </FieldValues>
          </Subscriber_Add_Subscriber>
        </soap:Body>
      </soap:Envelope>
    </cfoutput>
    </cfsavecontent>
        
    <cfhttp url="http://api7.publicaster.com/Subscriber/Subscriber.asmx?wsdl" method="POST" resolveurl="NO" throwonerror="yes" useragent="Axis/1.1">
        <cfhttpparam type="header" name="SOAPAction" value="http://BlueSkyFactory.Publicaster7.LegacyAPI/Subscriber/Subscriber_Add_Subscriber"> 
        <cfhttpparam type="xml" name="body" value="#localscope.soapRequest#">
    </cfhttp>
    <cfset localscope.soapresponse = XMLParse(cfhttp.FileContent) />

		<cfset msg = "Thank you for signing up!<br />">
		<cfset success = 1>
	</cfif>
</cfif>
<script>
function validateSignupForm(frm) {
	var emailRX = /^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/;
	var dateRX = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
	var zipRX = /^\d{5}$/ ;
	var msg = "";
		if (frm.email.value == "" || emailRX.test(frm.email.value) != true) {
			msg += "Please enter a valid email<br/>";
		}
		if (msg == "") {	return true;} 
		else {
				msg += "<br/>";
				document.getElementById('ferror').innerHTML = msg; 
				return false;}
		}
</script>




<div class="scrolling">

<!--- DEALER MOBILE STICKY NAV ---> 
	<section class="mobile-sticky">
		<img class="hamburger" src="images/hamburger.png" alt="open">
		<img class="close" src="images/close.png" alt="close">
		<img src="images/sticky-mobile.png" alt="Surgeon Rx2" style="width:100%!important;">
		<ul class="double">
			<li><a class="header_find_a_dealer" href="http://www.stx.com/surgeon-rx2-hockey-stick/" target="_blank" onClick="ga('send', 'event', { eventCategory: 'surgeon rx2 - header - clicked - find a dealer cta', eventAction: 'clicked', eventLabel: 'find a dealer cta'});">LEARN MORE</a></li>
			<li><a class="header_buy_now" href="#openModalBuy" onclick="ga('send', 'event', { eventCategory: 'surgeon rx2 - header - clicked - buy now cta', eventAction: 'clicked', eventLabel: 'buy now cta'});">BUY NOW</a></li>
		</ul>
<!---
		<ul class="final">
			<li><a href="https://www.instagram.com/stxhockey/" target="_blank">#ReshapeTheGame</a></li>
		</ul>
--->
	</section> <!-- /mobile-sticky -->
<!--- END DEALER --->

	<section class="header bg-img">	
<!--- DEALER DESKTOP SIDE NAV --->
<!--- This side-nav must be wrapped in a div with a class .header --->
		<div class="side-nav">
			<img class="logo" src="images/surgeon-logo.png"/>
			<a class="header_find_a_dealer" href="http://www.stx.com/surgeon-rx2-hockey-stick/" target="_blank" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-rx2-learn-more'});">Learn More</a>
			<a class="header_buy_now" href="#openModalBuy" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-rx2-buy-now'});">BUY NOW!</a>
		</div>	
<!--- END DEALER ---> 
<!--- MODAL --->
		<div id="openModalBuy" class="modalDialog">
			<div>
				<a href="#close" title="Close" class="close-modal"><img src="images/modal-close.png"></a>
				<p class="modal-buy-title">Buy Your Surgeon Rx2 Stick Today!</p>
			    <img class="divider" src="images/divider.png">
			    <div class="col">
				    <img class="divider pushed" src="images/hm.png">
				    <h3>Hockey Monkey</h3>
				    <a href="http://www.hockeymonkey.com/stx-surgeon-rx2-hockey-sticks.html" target="_blank" onClick="ga('send', 'event', { eventCategory: 'surgeon rx2 - pop-up - clicked - hockey monkey pre-order now', eventAction: 'clicked', eventLabel: 'hockey monkey pre-order now'});">BUY NOW</a>
			    </div>
			    <div class="col central">
				    <img class="divider" src="images/hw.png">
				    <h3>Perani's</h3>
				    <a href="http://www.hockeyworld.com/STX-Surgeon-RX2-Stick-Line" target="_blank" onClick="ga('send', 'event', { eventCategory: 'surgeon rx2 - pop-up - clicked - peranis hockey world pre-order now', eventAction: 'clicked', eventLabel: 'peranis hockey world pre-order now'});">BUY NOW</a>
			    </div>

			</div>
		</div>
<!--- END MODAL ---> 	

	
		<div class="container">
			<div class="title">
				<img class="logo mobile" src="images/logo.png" />
				<h1>Becoming 68</h1>
				<div class="vert-line mobile-hide"></div>
				<hr class="mobile"/>
				<h5>Go inside the lives of America's <br><strong>top 68 young hockey players</strong></h5>
			</div>
			<div class="feat-content">	
				<img class="logo mobile-hide" src="images/logo.png" />
				<a href="https://www.youtube.com/watch?v=6llPrLSbx74" target="_blank" class="html5lightbox"><img class="hover-effect2" id="play-btn-header" src="images/feat-thumb.png"/></a>
			</div>
			<div class="sponsors">
				<div class="container">
					<p>Becoming 68 presented by</p>
					<img src="images/sponsor-200x85com.png" />
				</div>
			</div>
		</div>
	</section>
	<!--- Header: end ---> 
	
	<!--- EMAIL FORM --->
    	<form method="post" id="email-subscription" action="#signup" onsubmit="return validateSignupForm(this);">
            <section class="email-subscribe">
                <div class="container">
                    <p>Be the first to know when new episodes drop.</p>
                    <!--- Form Error handling add to container with form. style as needed --->
                    <div id="ferror" style="color:red;"><cfif isDefined('msg')><cfoutput><p>#msg#</p></cfoutput></cfif></div>
		                <input type="text" name="email" placeholder="Email *"/>
                        <!--- Form Error handling add to container with form. style as needed --->
                        <div style="display: none"><input type="text" name="contact_name" value=""></div>
                        <input type="submit" name="submit" value="SIGN UP" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-email-signup'});">
                </div>
            </section>
    	</form>
    
	<!--- Email Subscribe: end --->
	
	
	
	
	<section class="episodes bg-img">
		<div class="container">
			<h2 class="headline">EPISODES</h2>
			<div class="grid">
				<div class="row">
					<div id="trailer" class="col-3 episode">
						<!--- to activate video hover effects remove x in "xhover-effect-1" --->
						<!--- also remove style="cursor:default" from anchor tags --->
						<a href="https://www.youtube.com/watch?v=6llPrLSbx74" target="blank_" class="wrap html5lightbox" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-watch-trailer'});"> 
							<div id="trlr" class="feat-img hover-effect1"><!--- <h4>10.12</h4> ---></div>
							<h6><span class="red">Trlr.</span> Becoming 68</h6>
						</a>
					</div>
					
					<div id="episode-1" class="col-3 episode">
						<a style="cursor:default;" target="blank_" class="wrap xhtml5lightbox" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-watch-episode1'});">
							<div id="ep-1" class="feat-img xhover-effect1"><h4>10.21</h4></div>
							<h6><span class="red">EP 1.</span> Coming Soon...</h6>
						</a>
					</div>
					<div id="episode-2" class="col-3 episode">
						<a style="cursor:default;" target="blank_" class="wrap xhtml5lightbox" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-watch-episode2'});">
							<div id="ep-2" class="feat-img xhover-effect1"><h4>10.28</h4></div>
							<h6><span class="red">EP 2.</span> Coming Soon...</h6>
						</a>
					</div>

					<div id="episode-3" class="col-3 episode">
						<a style="cursor:default;" target="blank_" class="wrap xhtml5lightbox" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-watch-episode3'});">
							<div id="ep-3" class="feat-img xhover-effect1"><h4>11.04</h4></div>
							<h6><span class="red">EP 3.</span> Coming Soon...</h6>
						</a>
					</div>
					<div id="episode-4" class="col-3 episode ">
						<a style="cursor:default;" target="blank_" class="wrap xhtml5lightbox" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-watch-episode4'});"> 
							<div id="ep-4" class="feat-img xhover-effect1"><h4>11.11</h4></div>
							<h6><span class="red">EP 4.</span> Coming Soon...</h6>
						</a>
					</div>
					<div id="episode-5" class="col-3 episode">
						<a style="cursor:default;" target="blank_" class="wrap xhtml5lightbox" onClick="ga('send', 'event', { eventCategory: 'becoming-68', eventAction: 'click', eventLabel: 'becoming-68-watch-episode5'});">
							<div id="ep-5" class="feat-img xhover-effect1"><h4>11.18</h4></div>
							<h6><span class="red">EP 5.</span> Coming Soon...</h6>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--- Episodes: end ---> 
	
	
	
	
	<section class="roster bg-img">
		<div class="container">
			<center><span><h2 class="headline">The STX 68</h2></span></center>
			
			<!-- DESKTOP ONLY -->
			<div class="grid desktop">
				<div class="col-4">
					<div class="wrap">
					<h6>Red Team</h6>
					<ul>
						<li>Nick Addante</li>
						<li>Kyle Aucoin</li>
						<li>Matthew Basgall</li>
						<li>Tucker Hartman</li>
						<li>Christian Jimenez</li>
						<li>Sam Saccone</li>
						<li>jet lee swift</li>
						<li>luke antonacci</li>
						<li>aidan cobb</li>
						<li>nick desantis</li>
						<li>Ryan Kirwan</li>
						<li>Matthew Knies</li>
						<li>Blake Mesenburg</li>
						<li>Ryan Mulrenin</li>
						<li>Isaac Novak</li>
						<li>Garrett Szydlowski</li>
						<li>Evan Ibbitson</li>
					</ul>
					</div>
				</div>
				<div class="col-4">
					<div class="wrap">
					<h6>Black Team</h6>
					<ul>
						<li>Colin Berke</li>
						<li>Zach Bookman</li>
						<li>Nick Donato</li>
						<li>Ryan MacDermott</li>
						<li>Martin Marnausz</li>
						<li>Nicholas Ohanisain</li>
						<li>Mason Proskin</li>
						<li>Michael Cameron</li>
						<li>Brandon Chabrier</li>
						<li>Colin Graf</li>
						<li>Mitchell Kohner</li>
						<li>Kyle Smolen</li>
						<li>Wyatt Schingoethe</li>
						<li>Tiernan Shoudy</li>
						<li>Luke Tuch</li>
						<li>Tbone Weis</li>
						<li>Ethan Mork</li>
					</ul>
					</div>
				</div>
				<div class="col-4">
					<div class="wrap">
					<h6>White Team</h6>
					<ul>
						<li>Drew Commesso</li>
						<li>Nick Degaetani</li>
						<li>Lachlan Getz</li>
						<li>Victor Mancini</li>
						<li>Thomas Messineo</li>
						<li>Andy Ramsey</li>
						<li>Casey Roepke</li>
						<li>Gabriel Bennett</li>
						<li>Seamus Campbell</li>
						<li>Hunter Hastings</li>
						<li>Cade Lemmer</li>
						<li>Jacob Perreault</li>
						<li>Hunter Rossi</li>
						<li>Patrick Schmiedlin</li>
						<li>Antonio Stranges</li>
						<li>Cole Vallese</li>
						<li>Carsen Stokes</li>
					</ul>
					</div>
				</div>
				<div class="col-4">
					<div class="wrap">
					<h6>Grey Team</h6>
					<ul>
						<li>Gavin Fitzpatrick</li>
						<li>Frank Djurasevic</li>
						<li>Luke Gramer</li>
						<li>Gerard Keane</li>
						<li>Jeffrey Lee</li>
						<li>Eamon Powell</li>
						<li>Jacob Truscott</li>
						<li>Tristan Broz</li>
						<li>Anthony Cipollone</li>
						<li>Jimmy Doyle</li>
						<li>Drew Holt</li>
						<li>Peter Lajoy</li>
						<li>Landon Slaggert</li>
						<li>Sasha Teleguine</li>
						<li>Zachary Tonelli</li>
						<li>Philip Tresca</li>
						<li>Tucker Tynan</li>
					</ul>
					</div>
				</div>
			</div>
			
			<!-- MOBILE ONLY --> 
			<div class="grid mobile" id="accordion">
				<h6 class="accordion-toggle">Red Team</h6>
				<div class="accordion-content default">
					<ul >
						<li>Nick Addante</li>
						<li>Kyle Aucoin</li>
						<li>Matthew Basgall</li>
						<li>Tucker Hartman</li>
						<li>Christian Jimenez</li>
						<li>Sam Saccone</li>
						<li>jet lee swift</li>
						<li>luke antonacci</li>
						<li>aidan cobb</li>
						<li>nick desantis</li>
						<li>Ryan Kirwan</li>
						<li>Matthew Knies</li>
						<li>Blake Mesenburg</li>
						<li>Ryan Mulrenin</li>
						<li>Isaac Novak</li>
						<li>Garrett Szydlowski</li>
						<li>Evan Ibbitson</li>
					</ul>
				</div>
				<h6 class="accordion-toggle">Black Team</h6>
				<div class="accordion-content">
					<ul>
						<li>Colin Berke</li>
						<li>Zach Bookman</li>
						<li>Nick Donato</li>
						<li>Ryan MacDermott</li>
						<li>Martin Marnausz</li>
						<li>Nicholas Ohanisain</li>
						<li>Mason Proskin</li>
						<li>Michael Cameron</li>
						<li>Brandon Chabrier</li>
						<li>Colin Graf</li>
						<li>Mitchell Kohner</li>
						<li>Kyle Smolen</li>
						<li>Wyatt Schingoethe</li>
						<li>Tiernan Shoudy</li>
						<li>Luke Tuch</li>
						<li>Tbone Weis</li>
						<li>Ethan Mork</li>
					</ul>
				</div>
				<h6 class="accordion-toggle">White Team</h6>
				<div class="accordion-content">
					<ul>
						<li>Drew Commesso</li>
						<li>Nick Degaetani</li>
						<li>Lachlan Getz</li>
						<li>Victor Mancini</li>
						<li>Thomas Messineo</li>
						<li>Andy Ramsey</li>
						<li>Casey Roepke</li>
						<li>Gabriel Bennett</li>
						<li>Seamus Campbell</li>
						<li>Hunter Hastings</li>
						<li>Cade Lemmer</li>
						<li>Jacob Perreault</li>
						<li>Hunter Rossi</li>
						<li>Patrick Schmiedlin</li>
						<li>Antonio Stranges</li>
						<li>Cole Vallese</li>
						<li>Carsen Stokes</li>
					</ul>
				</div>
				<h6 class="accordion-toggle">Grey Team</h6>
				<div class="accordion-content">
					<ul>
						<li>Gavin Fitzpatrick</li>
						<li>Frank Djurasevic</li>
						<li>Luke Gramer</li>
						<li>Gerard Keane</li>
						<li>Jeffrey Lee</li>
						<li>Eamon Powell</li>
						<li>Jacob Truscott</li>
						<li>Tristan Broz</li>
						<li>Anthony Cipollone</li>
						<li>Jimmy Doyle</li>
						<li>Drew Holt</li>
						<li>Peter Lajoy</li>
						<li>Landon Slaggert</li>
						<li>Sasha Teleguine</li>
						<li>Zachary Tonelli</li>
						<li>Philip Tresca</li>
						<li>Tucker Tynan</li>
					</ul>
				</div>
				
			</div>
		</div>
	</section>
	<!--- Roster: end ---> 
	
	<section id="cycle-slideshow-1">
		<div class="cycle-slideshow"  
			data-cycle-fx="scrollHorz"
			data-cycle-timeout="2000"
			data-cycle-prev="#prev"
			data-cycle-next="#next"
			
			><!-- close div tag -->
			<div class="cycle-pager" style=""></div>
			
			<img src="images/slide-1.jpg">
			<img src="images/slide-2.jpg">
			<img src="images/slide-3.jpg">
			<img src="images/slide-4.jpg">
			<img src="images/slide-5.jpg">
			<img src="images/slide-6.jpg">
			<img src="images/slide-7.jpg">
			<img src="images/slide-8.jpg">
			<img src="images/slide-9.jpg">
			
		</div>
		<div id="slide-nav-backdrop"></div>
		<div class="center" id="custom-cycle-pager">
		    <a href=# id="prev"><img src="images/prev-slide.png"/></a> 
		    <a href=# id="next"><img src="images/next-slide.png" /></a>
		</div>
	</section>
	<!--- Slideshow: end ---> 
	
	<section class="social">
		<p>SHARE</p>
		<div class="icons"> 
			<a href="https://www.facebook.com/sharer.php?u=http://dev.stx.com/becoming-68/" onclick="" target="blank_"><div class="icon" id="fb-icon"></div></a>
			<cfoutput>
			<a href="#twitterDescription#" onclick="" target="blank_"><div class="icon" id="tw-icon"></div></a>
			<a href="mailto:?subject=#subject#&body=#body#" onclick="" ><div class="icon" id="email-icon"></div></a>
			</cfoutput>
		</div>
	</section>
	<!--- Social: end --->
	
	<div class="preload">
		<img  src="images/trlr-hover.jpg"/>
		<img  src="images/ep-1-hover.jpg"/>
		<img  src="images/ep-2-hover.jpg"/>
		<img  src="images/ep-3-hover.jpg"/>
		<img  src="images/ep-4-hover.jpg"/>
		<img  src="images/ep-5-hover.jpg"/>
		<img src="images/tw-hover.png"/>
		<img src="images/fb-hover.png"/>
		<img src="images/email-hover.png"/png>
	</div> 	
</div>
<!-- end scrolling -->


<!-- JS -->
<script type="text/javascript">
  $(document).ready(function($) {
    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');

      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');

    });
  });
  
	  // secondary nav     
    $('.hamburger').click(function() {
        $('.mobile-sticky ul').css('display','block');
        $('.close').css('display','block');
        $('.hamburger').css('display','none');
    });
    $('.close').click(function() {
        $('.mobile-sticky ul').css('display','none');
        $('.close').css('display','none');
        $('.hamburger').css('display','block');
    });
</script>
<style>
  .accordion-toggle {cursor: pointer;}
  .accordion-content {display: none;}
  .accordion-content.default {display: block;}
</style>

