<cfsetting showdebugoutput="yes">

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name="robots" content="index,follow">
		<meta name="revisit-after" content="14 days">
		<meta name="googlebot" content="index,follow">
		<meta charset="UTF-8">
		<cfset request.metaTitle = "STX Hockey Becoming 68">
		<cfset request.metaDesc = "Go inside the lives of America's Top 68 young hockey players with STX Hockey and watch the Becoming 68 series!">
		<meta property="og:type" content="website">
		<meta property="og:title" content=""/>
		<meta property="og:description" content="Go inside the lives of America's Top 68 young hockey players with STX Hockey and watch the Becoming 68 series!" />
		<meta property="og:url" content="http://dev.stx.com/becoming-68/" />
		<cfset request.metaImage = "http://dev.stx.com/becoming-68/images/becoming-68-share-image.jpg"/>

		<meta name="viewport" content="width=device-width, initial-scale=.8">
	    <link rel="stylesheet" href="/css/styles.css" />
	    <link rel="stylesheet" href="/becoming-68/css/styles.css" />
	    <link href="/becoming-68/css/jquery.bxslider.css" rel="stylesheet" />
	    <link rel="stylesheet" href="/css/jquery.prettyPhoto.css" />
		
		<!-- Typography.com: Tungsten -->
		<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6574652/7828972/css/fonts.css" />
		
		<!-- Typekit: Klavika -->
	    <script type="text/javascript" src="//use.typekit.net/smb2aua.js"></script>
	    
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	    <script src="/js/jquery.prettyPhoto.js"></script>        
		<script language="javascript" type="text/javascript" src="/js/jquery.masonry.min.js"></script>
        <script language="javascript" type="text/javascript" src="/js/jquery-ui.min.js"></script>
        <script language="javascript" type="text/javascript" src="/becoming-68/js/scripts.js"></script>
		<script language="javascript" type="text/javascript" src="/js/scripts.js"></script>
	    <script type="text/javascript">
	    	$(function(){
		    	$("a[rel^='prettyPhoto']").prettyPhoto({
			    	'deeplinking':false,
			    	'social_tools':false,
			    	'default_width': 640,
			    	'default_height': 360
		    	});
	    	});
	    </script>
		<!--[if lt IE 8]>
		<script src="/ie7/IE8.js" type="text/javascript"></script>
		<![endif]-->
		<!--[if lt IE 7]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js">IE7_PNG_SUFFIX=".png";</script>
		<![endif]-->
		<!--generate all new elements for Internet Explorer-->
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body class="becoming-68 page-<cfoutput>#url.page#</cfoutput>">
    	<cfinclude template="/includes/menu_trigger.cfm">
	  <div id="overlay"></div>
	<div id="wrapped">
		<cfinclude template="/includes/header_mobile.cfm">
        <cfset request.sport = "ice">
		<cfset usehash = false>
		<cfset request.gender = 'hockey'>
		<cfset request.genderid = 0>
		<div id="wrapper">
			<cfinclude template="/includes/header.cfm">
		</div>
		<div id="main">
			<cfinclude template="display/dsp_#url.page#.cfm">
		</div>		
		<div id="wrapper" class="mobile-hide">	
			<cfinclude template="/includes/footer.cfm">
			<div class="clearfix"></div>
		</div>
	</div>
		<cfinclude template="/includes/mobile-menu.cfm">
		<cfinclude template="/modules/mod_meta.cfm">
	</body>
	<script type="text/javascript" src="/becoming-68/js/html5lightbox.js"></script>
	<script language="javascript" type="text/javascript" src="/becoming-68/js/jquery.cycle2.min.js"></script>
	<script type="text/javascript">
        $(window).resize(function() {
            checkWindowSize();
        });
        checkWindowSize();
        function checkWindowSize() {
            if ($(window).width() <= 640) {
                jQuery("a[rel^='prettyPhoto']").unbind('click');
            } else {
                $("a[rel^='prettyPhoto']").prettyPhoto({
                    social_tools: false,
                    deeplinking: false
                });
                jQuery("a[rel^='prettyPhoto']").bind('click');
            }
        }
    </script>
</html>